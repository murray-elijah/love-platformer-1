local Class = require 'libs.hump.class'
local Entity = require 'entities.Entity'

local player = Class{
  __includes = Entity
}

function player:init(world, x, y)
  self.img = love.graphics.newImage('/assets/character_block.png')
  
  Entity.init(self, world, x, y, self.img:getWidth(), self.img:getHeight())
  
  self.xV = 0
  self.yV = 0
  self.acc = 100
  self.maxSpeed = 600
  self.friction = 20
  self.gravity = 80
  
  self.isJumping = false
  self.isGrounded = false
  self.hasReachedMax = false
  self.jumpAcc = 500
  self.jumpMaxSpeed = 11
  
  self.world:add(self, self:getRect())
end

function player:collisionFilter(other)
  local x, y, w, h = self.world:getRect(other)
  local playerBottom = self.y + self.h
  local otherBottom = y + h
  
  if playerBottom <= y then
    return 'slide'
  end
end

function player:update(dt)
  local prevX, prevY = self.x, self.y
  
  self.xV = self.xV * (1 - math.min(dt * self.friction, 1))
  self.yV = self.yV * (1 - math.min(dt * self.friction, 1))
  
  self.yV = self.yV + self.gravity * dt
  
  if love.keyboard.isDown("left", "a") and self.xV > -self.maxSpeed then
    self.xV = self.xV - self.acc * dt
  elseif love.keyboard.isDown("right", "d") and self.xV < self.maxSpeed then
    self.xV = self.xV + self.acc * dt
  end
  
  if love.keyboard.isDown("up", "w") then
    if -self.yV < self.jumpMaxSpeed and not self.hasReachedMax then
      self.yV = self.yV - self.jumpAcc * dt
    elseif math.abs(self.yV) > self.jumpMaxSpeed then
      self.hasReachedMax = true
    end
    
    self.isGrounded = false
  end
  
  local goalX = self.x + self.xV
  local goalY = self.y + self.yV
  
  self.x, self.y, collisions, len = self.world:move(self, goalX, goalY, self.collisionFilter)
  
  for i, coll in ipairs(collisions) do
    if coll.touch.y > goalY then
      self.hasReachedMax = true
      self.isGrounded = false
    elseif coll.normal.y < 0 then
      self.hasReachedMax = false
      self.isGrounded = true
    end
  end
end

function player:draw()
  love.graphics.draw(self.img, self.x, self.y)
end

return player